import os

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QApplication, QGridLayout, QLabel, QLineEdit, QPushButton, QFileDialog, QMessageBox

import lyj_progressBar, sys
from MD5Thread import MD5Thread


class MyFrom(QWidget):
    def __init__(self, parent=None):
        super(MyFrom, self).__init__(parent=parent)
        self.setWindowTitle('MD5图片修改程序')
        self.resize(600, 200)
        self.init()

    def init(self):
        self.gridlayout = QGridLayout()
        self.label1 = QLabel('选择需要转换MD5图片的目录：')
        self.gridlayout.addWidget(self.label1, 0, 0, 2, 1)
        self.lineEdit1 = QLineEdit()
        self.lineEdit1.setEnabled(False)
        self.gridlayout.addWidget(self.lineEdit1, 0, 2, 2, 1)
        self.button1 = QPushButton('选择图片目录')
        self.gridlayout.addWidget(self.button1, 0, 4, 2, 1)
        self.submitButton = QPushButton('批量修改图片MD5值')
        self.gridlayout.addWidget(self.submitButton, 3, 2, 2, 1)
        self.button1.clicked.connect(self.button_connect)
        self.submitButton.clicked.connect(self.submitButton_connect)

        self.md5Thread = MD5Thread()
        self.md5Thread._signal.connect(self.md5Thread_callbacklog)
        self.md5Thread._parValue.connect(self.callbacklog_par)
        self.setLayout(self.gridlayout)

    def callbacklog_par(self, value):
        self.ui.set_progressBar(value)

    def md5Thread_callbacklog(self, result):
        self.button1.setEnabled(True)
        self.submitButton.setEnabled(True)
        self.Dialog.close()
        if result:
            QMessageBox.question(self, '消息', '修改图片MD5成功',
                                 QMessageBox.Yes)
        else:
            QMessageBox.question(self, '消息', '修改图片MD5失败',
                                 QMessageBox.Yes)

    def button_connect(self):
        fileName = QFileDialog.getExistingDirectory(self, "选取文件夹", 'D:/')
        self.lineEdit1.setText(fileName)

    def submitButton_connect(self):
        fileName = self.lineEdit1.text()
        self.md5Thread.setValue(fileName)
        self.md5Thread.start()
        self.button1.setEnabled(False)
        self.submitButton.setEnabled(False)
        self.Dialog = QtWidgets.QDialog()
        self.ui = lyj_progressBar.Ui_Dialog()
        self.ui.setupUi(self.Dialog)
        self.Dialog.show()
        self.Dialog.exec()


if '__main__' == __name__:
    app = QApplication(sys.argv)
    myUI = MyFrom()
    myUI.setWindowFlag(Qt.WindowMinimizeButtonHint)  # 禁止放大界面
    myUI.setFixedSize(myUI.width(), myUI.height())  # 静止拖拽放大界面
    myUI.show()
    sys.exit(app.exec_())
    os.system("pause")
