import os
import time

from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal


class MD5Thread(QtCore.QThread):
    _signal = pyqtSignal(bool)
    _parValue = pyqtSignal(int)

    def __init__(self):
        super(MD5Thread, self).__init__()

    def setValue(self, fileName):
        self.fileName = fileName

    def run(self):
        try:
            self._parValue.emit(50)
            self.updateMD5(self.fileName)
            self._parValue.emit(100)
            self._signal.emit(True)
        except Exception:
            self._signal.emit(False)

    def updateMD5(self, dirpath):
        writefile = int(time.time() * 1000)
        filenames = os.walk(dirpath)
        for root, dirs, files in filenames:
            for file in files:
                file = os.path.join(root, file)
                if file.endswith('.png') or file.endswith('.jpg') or file.endswith('jpeg'):
                    with open(file, "a") as f:
                        f.write(str(writefile))
